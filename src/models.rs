use diesel::{Queryable};

#[derive(Serialize, Queryable)]
pub struct Question {
    pub id: i32,
    pub name: String,
}
