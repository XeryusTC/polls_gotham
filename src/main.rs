#[macro_use]
extern crate diesel;
#[macro_use]
extern crate gotham_derive;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate tera;

use diesel::prelude::*;
use diesel::result::Error as DieselError;
use diesel::sqlite::SqliteConnection;

use futures::{future, Future};

use gotham::handler::{HandlerFuture, IntoHandlerError};
use gotham::helpers::http::response::create_response;
use gotham::state::{FromState, State};
use gotham::pipeline::new_pipeline;
use gotham::pipeline::single::single_pipeline;
use gotham::router::{builder::*, Router};
use gotham_middleware_diesel::DieselMiddleware;

use hyper::{Body, Response, StatusCode};
use tera::{Context, Tera};

pub mod schema;
pub mod models;

use models::Question;

/// Type that is a convenience for the sqlite connection
pub type Repo = gotham_middleware_diesel::Repo<SqliteConnection>;
/// Result of a Question select query
pub type QuestionResult = Result<Question, DieselError>;

lazy_static! {
    pub static ref TERA: Tera =
        compile_templates!(concat!(env!("CARGO_MANIFEST_DIR"),
                                   "/templates/**/*"));
}

#[derive(Deserialize, StateData, StaticResponseExtender)]
struct IdExtractor {
    id: i32,
}

/// Displays the polls
pub fn get_polls(state: State) -> Box<HandlerFuture> {
    use crate::schema::questions::dsl::*;

    let repo = Repo::borrow_from(&state).clone();
    let f = repo
        .run(move |conn| questions.load::<Question>(&conn))
        .then(|result| match result {
            Ok(polls) => {
                let mut context = Context::new();
                context.insert("polls", &polls);
                let rendered = TERA.render("index.html", &context).unwrap();
                let res = create_response(&state,
                                          StatusCode::OK,
                                          mime::TEXT_HTML,
                                          rendered);
                future::ok((state, res))
            }
            Err(e) => future::err((state, e.into_handler_error()))
        });

    Box::new(f)
}

/// Displays a single poll
pub fn get_poll(state: State) -> Box<HandlerFuture> {
    use crate::schema::questions::dsl::*;

    let poll_id = IdExtractor::borrow_from(&state);
    let poll_id = poll_id.id;
    let repo = Repo::borrow_from(&state).clone();
    let f = repo
        .run(move |conn|
             questions
                 .filter(id.eq(poll_id))
                 .first(&conn)
        )
        .then(|result: QuestionResult| match result {
            Ok(poll) => {
                let mut context = Context::new();
                context.insert("poll", &poll);
                let rendered = TERA
                    .render("poll.html", &context)
                    .unwrap();
                let res = create_response(&state,
                                          StatusCode::OK,
                                          mime::TEXT_HTML,
                                          rendered);
                future::ok((state, res))
            }
            Err(DieselError::NotFound) => future::ok(response_404(state)),
            Err(e) => future::err((state, e.into_handler_error())),
        });

    Box::new(f)
}

/// Renders a 404 template
pub fn response_404(state: State) -> (State, Response<Body>) {
    let rendered = TERA.render("404.html", &Context::new()).unwrap();
    let res = create_response(
        &state,
        StatusCode::NOT_FOUND,
        mime::TEXT_HTML,
        rendered);
    (state, res)
}

fn router(repo: Repo) -> Router {
    let (chain, pipeline) = single_pipeline(
        new_pipeline().add(DieselMiddleware::new(repo)).build());

    build_router(chain, pipeline, |route| {
        route.get("/").to(get_polls);
        route
            .get("/poll/:id")
            .with_path_extractor::<IdExtractor>()
            .to(get_poll);
    })
}

fn main() {
    println!("Establishing database connection");
    let repo = Repo::new("polls.db");

    let addr = "localhost:7878";
    println!("Listening for requests at http://{}", addr);
    gotham::start(addr, router(repo))
}
